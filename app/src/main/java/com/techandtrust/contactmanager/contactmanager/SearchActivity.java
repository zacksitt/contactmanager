package com.techandtrust.contactmanager.contactmanager;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.techandtrust.contactmanger.adapter.AllContactsAdapter;
import com.techandtrust.contactmanger.adapter.ContactsAdapter;
import com.techandtrust.db.ContactDataSource;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = SearchActivity.class.getSimpleName();
    private  RecyclerView rvMpt_layout;
    private ArrayList<Contact> contacts;
    private Contact contact;
    private AllContactsAdapter contactsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setUpSetting();
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        rvMpt_layout = (RecyclerView) findViewById(R.id.allContacts_Rview);
        initArrAdapter();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        searchView.requestFocus();

        searchView.setOnSearchClickListener(new SearchView.OnClickListener(){

            @Override
            public void onClick(View v) {

            }

        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e(TAG, "Submit:"+query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText==""){
                    initArrAdapter();
                }else{
                    searchItem(newText);
                }
                return false;
            }
        });


    }

    public void setUpSetting(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String themePref = sharedPref.getString(MainActivity.KEY_PREF_THEME, "");
        String fontsizePref = sharedPref.getString(MainActivity.KEY_PREF_FONT_SIZE, "");
        Log.e(TAG,"CHECK SETTING");
        Log.e(TAG,themePref);
        Log.e(TAG,fontsizePref);
        if(themePref.equals("light")){
            setTheme(R.style.AppLightTheme);
        }
        else{
            setTheme(R.style.AppTheme);
        }

    }

    public  void searchItem(String s){

        ArrayList<Contact> newContactArr = new ArrayList<>();
        for (int i =0;i<contacts.size();i++){
            contact = contacts.get(i);
            if(containsIgnoreCase(contact.getName(),s)){
                newContactArr.add(contact);
            }
            if(containsIgnoreCase(contact.getPhone(),s)){
                newContactArr.add(contact);
            }
        }
        AllContactsAdapter contactsAdapter = new AllContactsAdapter(newContactArr,SearchActivity.this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.allContacts_Rview);
        recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
        recyclerView.setAdapter(contactsAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
//            case R.id.action_setting:
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.searchmenu, menu);
        return true;
    }

    private boolean containsIgnoreCase(String str, String searchStr)     {
        if(str == null || searchStr == null) return false;

        final int length = searchStr.length();
        if (length == 0)
            return true;

        for (int i = str.length() - length; i >= 0; i--) {
            if (str.regionMatches(true, i, searchStr, 0, length))
                return true;
        }
        return false;
    }

    private void initArrAdapter(){

        ContactDataSource source = new ContactDataSource(this);
        source.open();
        contacts = new ArrayList<>();
        contacts = source.getall();
        contactsAdapter = new AllContactsAdapter(contacts,this);
        rvMpt_layout.setLayoutManager(new LinearLayoutManager(this));
        rvMpt_layout.setAdapter(contactsAdapter);
    }
}
