package com.techandtrust.contactmanager.contactmanager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.techandtrust.contactmanger.adapter.AllContactsAdapter;
import com.techandtrust.contactmanger.adapter.ContactsAdapter;
import com.techandtrust.db.ContactDataSource;

import java.util.ArrayList;

/**
 * Created by ICE on 05-Jun-17.
 */

public class AllContactFragment extends Fragment {

    private ArrayList<Contact> contacts;
    private Contact contact;
    private RecyclerView rvMpt_layout;
    private AllContactsAdapter contactsAdapter;
    private final String TAG = "AllContactFragment";
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.allcontact_frag, container, false);
        rvMpt_layout = (RecyclerView) view.findViewById(R.id.allContacts_Rview);
        progressBar = (ProgressBar) view.findViewById(R.id.loadingProgress);
        progressBar.setVisibility(View.VISIBLE);
        initArrAdapter();

        return view;
    }

    public  void searchItem(String s){

        ArrayList<Contact> newContactArr = new ArrayList<>();
        for (int i =0;i<contacts.size();i++){
            contact = contacts.get(i);
            if(containsIgnoreCase(contact.getName(),s)){
                newContactArr.add(contact);
            }
        }
        contactsAdapter = new AllContactsAdapter(newContactArr,getActivity());
        rvMpt_layout.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMpt_layout.setAdapter(contactsAdapter);
    }

    private boolean containsIgnoreCase(String str, String searchStr)     {
        if(str == null || searchStr == null) return false;

        final int length = searchStr.length();
        if (length == 0)
            return true;

        for (int i = str.length() - length; i >= 0; i--) {
            if (str.regionMatches(true, i, searchStr, 0, length))
                return true;
        }
        return false;
    }
    private void initArrAdapter(){
        ContactDataSource source = new ContactDataSource(getActivity());
        source.open();
        contacts = new ArrayList<>();
        contacts = source.getall();

        contactsAdapter = new AllContactsAdapter(contacts,getActivity());
        rvMpt_layout.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMpt_layout.setAdapter(contactsAdapter);
    }
}
