package com.techandtrust.contactmanager.contactmanager;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.techandtrust.contactmanger.adapter.ContactsAdapter;
import com.techandtrust.db.ContactDataSource;

import java.util.ArrayList;

/**
 * Created by ICE on 01-Jun-17.
 */

public class MptFragment extends Fragment {

    private ArrayList<Contact> contacts;
    private Contact contact;
    private RecyclerView rvMpt_layout;

    private final String TAG = "Mptfragement";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ContactDataSource source = new ContactDataSource(getActivity());
        source.open();
        View view = inflater.inflate(R.layout.mpt_frag, container, false);
        rvMpt_layout = (RecyclerView) view.findViewById(R.id.mptContacts_Rview);
        contacts = new ArrayList<>();
        contacts = source.getContacts("mpt");
        for (int i=0;i<contacts.size();i++){
            contact=contacts.get(i);
        }
        ContactsAdapter contactsAdapter = new ContactsAdapter(contacts,getActivity());
        rvMpt_layout.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMpt_layout.setAdapter(contactsAdapter);

        return view;
    }
}
