package com.techandtrust.contactmanager.contactmanager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class SettingFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    ListPreference themeListPref,fontsizeListPref;
    public static final String KEY_PREF_THEME="app_theme";
    public static final String KEY_PREF_FONT_SIZE="app_font_size";
    SharedPreferences sharedPreferences;
    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_general);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        Preference themePref = findPreference(KEY_PREF_THEME);
        themePref.setSummary(sharedPreferences.getString(KEY_PREF_THEME,""));

        Preference fontsizePref = findPreference(KEY_PREF_FONT_SIZE);
        fontsizePref.setSummary(sharedPreferences.getString(KEY_PREF_FONT_SIZE,""));

////        PreferenceManager.setDefaultValues(getActivity(),R.xml.pref_general,false);
//        themeListPref = (ListPreference) this.findPreference(KEY_PREF_THEME);
////        mListPreference = (ListPreference)  getPreferenceManager().findPreference(KEY_PREF_THEME);
//        themeListPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//            @Override
//            public boolean onPreferenceChange(Preference preference, Object newValue) {
//                // your code here
//                Log.e(" Setting fragment",newValue.toString());
//                Preference connectionPref = findPreference(KEY_PREF_THEME);
//                // Set summary to be the user-description for the selected value
//                connectionPref.setSummary(newValue.toString());
//
//                return false;
//            }
//
//        });
//
//        fontsizeListPref = (ListPreference)  getPreferenceManager().findPreference(KEY_PREF_FONT_SIZE);
//        fontsizeListPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//            @Override
//            public boolean onPreferenceChange(Preference preference, Object newValue) {
//                // your code here
//                Log.e(" Setting fragment",newValue.toString());
//                Preference connectionPref = findPreference(KEY_PREF_FONT_SIZE);
//                // Set summary to be the user-description for the selected value
//                connectionPref.setSummary(newValue.toString());
//                return false;
//            }
//
//        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if(s.equals(KEY_PREF_THEME)){
            Preference themePref = findPreference(KEY_PREF_THEME);
            // Set summary to be the user-description for the selected value
            themePref.setSummary(sharedPreferences.getString(KEY_PREF_THEME,""));
        }else{
            Preference fontsizePref = findPreference(KEY_PREF_FONT_SIZE);
            // Set summary to be the user-description for the selected value
            fontsizePref.setSummary(sharedPreferences.getString(KEY_PREF_FONT_SIZE,""));
        }

    }

}
