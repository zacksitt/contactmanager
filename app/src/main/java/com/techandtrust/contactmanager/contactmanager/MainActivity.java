package com.techandtrust.contactmanager.contactmanager;
import android.Manifest;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.techandtrust.contactmanger.adapter.TabPageAdapter;
import com.techandtrust.db.ContactDataSource;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements ActionBar.TabListener{

    private String[] tabs = { "All", "MPT", "Telenor","Ooreedoo" };
    public static final String KEY_PREF_THEME="app_theme";
    public static final String KEY_PREF_FONT_SIZE="app_font_size";
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int MY_PERMISSION_REQUEST_READ_CONTACT = 1000;
    private static final int MY_PERMISSION_REQUEST_CALL = 2000;

    private BottomNavigationView navigationView;
    private TabLayout tabLayout;
    public ProgressDialog progress;
    private ViewPager viewPager;
    private TabPageAdapter tabPageAdapter;
    private SharedPreferences sharedPref;
    private int firstNum = 0;
    private ActionBar actionBar;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);



        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            // Do something for lollipop and above versions
            setUpSetting(0);
            setContentView(R.layout.activity_main);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            Log.e("Main Activity","New Version");
        } else{
            // do something for phones running an SDK before lollipop
//            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//            setSupportActionBar(toolbar);
            setUpSetting(1);
            setContentView(R.layout.activity_main);
            actionBar = getSupportActionBar();
            actionBar.setNavigationMode(actionBar.NAVIGATION_MODE_TABS);
        }


        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        // initialize the Floating button<br />
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                intent.setData(Uri.parse("tel:"));
//                startActivity(intent);
//            }
//        });
        navigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.removeShiftMode(navigationView);
        navigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener(){

                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.action_add:
                                AddNew();
                                break;
                            case R.id.action_dial:
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:"));
                                startActivity(intent);
                                break;
                            case R.id.action_contacts:
                                contactPage();
                                break;
                            case R.id.action_log:
                                CallLog();
                                break;
                        }
                        return false;
                    }
                });
        progress = new ProgressDialog(this);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        if(this.checkContact()){
            fetchContactAsyn task = new fetchContactAsyn();
            task.execute();
        }
        this.startPager();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG," RESUME ");
    }
    public void setUpSetting(int num){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String themePref = sharedPref.getString(KEY_PREF_THEME, "");
        String fontsizePref = sharedPref.getString(KEY_PREF_FONT_SIZE, "");
        Log.e(TAG,"CHECK SETTING");
        Log.e(TAG,themePref);
        Log.e(TAG,fontsizePref);
        if(num==0){
            if(themePref.equals("light")){
                setTheme(R.style.AppLightTheme);
            }
            else{
                setTheme(R.style.AppTheme);
            }
        }else{
            if(themePref.equals("light")){
                setTheme(R.style.AppLightThemeOld);
            }
            else{
                setTheme(R.style.AppThemeOld);
            }
        }


    }

    public void startPager(){

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(3);
        tabPageAdapter = new TabPageAdapter(getSupportFragmentManager());
        tabPageAdapter.AddFragement(new AllContactFragment(), "");
        tabPageAdapter.AddFragement(new MptFragment(), "");
        tabPageAdapter.AddFragement(new TelenorFragment(), "");
        tabPageAdapter.AddFragement(new OreedooFragment(), "");

        viewPager.setAdapter(tabPageAdapter);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        // Adding Tabs
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            final ActionBar actionBar = getSupportActionBar();
            for (String tab_name : tabs) {
                actionBar.addTab(actionBar.newTab().setText(tab_name)
                        .setTabListener(this));
        }

            viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
                public void onPageSelected(int position){
                    actionBar.setSelectedNavigationItem(position);
                }
            });

        }else {
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.getTabAt(0).setIcon(R.drawable.all_icon);
            tabLayout.getTabAt(1).setIcon(R.drawable.mpt_icon);
            tabLayout.getTabAt(2).setIcon(R.drawable.telenor_icon);
            tabLayout.getTabAt(3).setIcon(R.drawable.ooreedoo_icon);


        }
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){

            case MY_PERMISSION_REQUEST_READ_CONTACT:
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    progress.setMessage("loading contact...");
                    progress.setCancelable(false);
                    progress.show();

                    fetchContactAsyn task = new fetchContactAsyn();
                    task.execute();
                }else{
                    Toast.makeText(this, "CONTACT READ PERMISSION FAIL!!!", Toast.LENGTH_LONG).show();
                }

                if(grantResults.length>0 && grantResults[1]==PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "GOT PHONE CALL PERMISSION", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(this, "PHONE CALL PERMISSION FAIL!!!", Toast.LENGTH_LONG).show();
                }

                break;

        }
    }

    private boolean checkContact(){

        SharedPreferences sharedPref = this.getPreferences(this.MODE_PRIVATE);
        int count = sharedPref.getInt("contact_count", 0);
        Contact contact = new Contact();
        ContactDataSource source = new ContactDataSource(this);
        source.open();
//        source.clearAll();
        ArrayList<Contact> contactArr = source.getall();

        if(contactArr.size()==0 || count > contactArr.size()){
            return true;

        }else {
            return false;
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    private class fetchContactAsyn extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            Contact contact = new Contact();
            ContactDataSource source = new ContactDataSource(MainActivity.this);
            source.open();
            ArrayList<Contact> contactArr = source.getall();
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,new String[]{
                        Manifest.permission.READ_CONTACTS,Manifest.permission.CALL_PHONE,Manifest.permission.SEND_SMS},MY_PERMISSION_REQUEST_READ_CONTACT);
//            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS},MY_PERMISSION_REQUEST_CALL);
            return " Permisssion Denied";
            }else{

                ContentResolver contentResolver = MainActivity.this.getContentResolver();
                Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
                Log.e(TAG, "CONTACt COUNT : " + contactArr.size());

                if (cursor.getCount() > 0) {
                    source.clearAll();
                    sharedPref = MainActivity.this.getPreferences(MainActivity.this.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("contact_count", cursor.getCount());

                    while (cursor.moveToNext()) {
                        int hasPhoneNum = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                        if (hasPhoneNum > 0) {
                            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                            Log.i(TAG, name);
                            contact = new Contact();
                            contact.setName(name);

                            Cursor phoneCursor = contentResolver.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{id},
                                    null);
                            if (phoneCursor.moveToNext()) {
                                String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                contact.setPhone(phoneNumber);
                                MainActivity.this.checkOperator(contact);
                            }

                            phoneCursor.close();

                            Cursor emailCursor = contentResolver.query(
                                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                                    new String[]{id}, null);
                            while (emailCursor.moveToNext()) {
                                String emailId = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                            }

                            MainActivity.this.checkOperator(contact);
                            source.add(contact);
                            Log.i(TAG, "INSERTE CONTACT" + contact.getName());
                            Log.i(TAG, "INSERTED CONTACT !!!!!!");
                        }
                    }
//
//            ContactsAdapter contactsAdapter = new ContactsAdapter(contacts,getActivity());
//            rvMpt_layout.setLayoutManager(new LinearLayoutManager(getActivity()));
//            rvMpt_layout.setAdapter(contactsAdapter);
                } else {
                    Log.e(TAG, "No New Contact !!!!!!");

                }

            }
            return "Download failed";

    }

        @Override
        protected void onPostExecute(String result) {
                startPager();
                progress.hide();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contactmenu, menu);

//        MenuItem item = menu.findItem(R.id.action_search);
//        SearchView searchView = (SearchView) item.getActionView();
//        searchView.setOnSearchClickListener(new SearchView.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//                Log.e(TAG, "On Click EVENT!!!!!!!!!!!!!");
//                tabLayout.setVisibility(View.GONE);
//                doSearch();
//            }
//        });
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
//
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                Log.e(TAG, "Submit:"+query);
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                Log.e(TAG, "Textchange:"+newText);
//                return false;
//            }
//        });
        return true;
    }

    private void CallLog() {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                ContactsContract.Contacts.CONTENT_URI);
        intent.setType(CallLog.Calls.CONTENT_TYPE);
        startActivity(intent);
    }

    private void AddNew() {
        Intent intent = new Intent(Intent.ACTION_INSERT,
                ContactsContract.Contacts.CONTENT_URI);
        startActivity(intent);

    }

    private void contactPage(){
        Intent i = new Intent();
        i.setComponent(new ComponentName("com.android.contacts", "com.android.contacts.DialtactsContactsEntryActivity"));
        i.setAction("android.intent.action.MAIN");
        i.addCategory("android.intent.category.LAUNCHER");
        i.addCategory("android.intent.category.DEFAULT");
        startActivity(i);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        Toast.makeText(this, "YARON YOREH 2", Toast.LENGTH_LONG).show();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Toast.makeText(this, "YARON YOREH asked for " + query, Toast.LENGTH_LONG).show();
            //use the query to search your data somehow
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {
//            case R.id.action_new:
//                AddNew();
//                return true;
//            case R.id.action_calllog:
//                CallLog();
//                return true;
//            case R.id.action_contacts:
//                contactPage();
//                return true;

            case R.id.action_search:
//                doSearch();
//                  onSearchRequested();
//                 onSearchRequested();
                Intent intent = new Intent(this, SearchActivity.class);
                startActivity(intent);
                return true;
//
            case R.id.action_setting:
                Intent i = new Intent(this, SettingActivity.class);
                startActivity(i);
                finish();
                return true;
            case R.id.action_about:
                Intent ii = new Intent(this, AboutActivity.class);
                startActivity(ii);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public Contact checkOperator(Contact contact) {

        if (contact.getPhone().length() >= 10) {
            contact.setPhone(contact.getPhone().replaceAll("[^\\d.]", ""));
            if(contact.getPhone().length() == 12) {
                firstNum = Integer.parseInt(contact.getPhone().substring(3,4));
            }else{
                firstNum = Integer.parseInt(contact.getPhone().substring(2,3));
            }

            if (firstNum == 7) {
                contact.setOperator("telenor");
            } else if(firstNum==0){
                contact.setOperator("other");
            }else if (firstNum == 9) {
                contact.setOperator("ooredoo");
            } else {
                contact.setOperator("mpt");
            }
//            Log.e(TAG, contact.getOperatorid()+"");
        }else{
            contact.setOperator("other");
        }
        return contact;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

}
