package com.techandtrust.contactmanager.contactmanager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class SettingActivity extends AppCompatActivity  {

    public static final String KEY_PREF_THEME="app_theme";
    public static final String KEY_PREF_FONT_SIZE="app_font_size";
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpSetting();
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        SettingFragment settingFrag = (SettingFragment) getFragmentManager()
//                                .findFragmentById(R.id.flayont_setting);

        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        getFragmentManager().beginTransaction()
                .replace(R.id.container_fragment,new SettingFragment())
                .commit();
    }

    public void setUpSetting(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String themePref = sharedPref.getString(KEY_PREF_THEME, "");
        String fontsizePref = sharedPref.getString(KEY_PREF_FONT_SIZE, "");
        Log.e(TAG,"CHECK SETTING");
        Log.e(TAG,themePref);
        Log.e(TAG,fontsizePref);
        if(themePref.equals("light")){
            setTheme(R.style.AppLightTheme);
        }
        else{
            setTheme(R.style.AppTheme);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.e(" SETTING ACTIVITY","OnBackPressed");
        // your code.
        startMainActivity();
    }

    public void startMainActivity(){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                startMainActivity();
                return true;
//            case R.id.action_setting:
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contactmenu, menu);
//        get menu item
        MenuItem item = menu.findItem(R.id.action_search);
        MenuItem item1 = menu.findItem(R.id.action_setting);
        MenuItem item2 = menu.findItem(R.id.action_about);
        // get menu item
        item.setVisible(false);
        item1.setVisible(false);
        item2.setVisible(false);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

}
