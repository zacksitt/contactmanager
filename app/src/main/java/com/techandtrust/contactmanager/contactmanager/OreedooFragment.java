package com.techandtrust.contactmanager.contactmanager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.techandtrust.contactmanger.adapter.ContactsAdapter;
import com.techandtrust.db.ContactDataSource;

import java.util.ArrayList;

/**
 * Created by ICE on 01-Jun-17.
 */

public class OreedooFragment extends Fragment {

    private ArrayList<Contact> contacts;
    private Contact contact;
    private RecyclerView rvMpt_layout;

    private final String TAG = "Oreedoo Fragement";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ContactDataSource source = new ContactDataSource(getActivity());
        source.open();
        View view = inflater.inflate(R.layout.oreedoo_frag, container, false);
        rvMpt_layout = (RecyclerView) view.findViewById(R.id.mptContacts_Rview);
        contacts = new ArrayList<>();
        contacts = source.getContacts("ooredoo");
        for (int i=0;i<contacts.size();i++){
            contact=contacts.get(i);
        }
        ContactsAdapter contactsAdapter = new ContactsAdapter(contacts,getActivity());
        rvMpt_layout.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMpt_layout.setAdapter(contactsAdapter);

        return view;
    }
}
