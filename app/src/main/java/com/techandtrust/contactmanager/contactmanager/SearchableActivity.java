package com.techandtrust.contactmanager.contactmanager;

import android.app.Activity;
import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.techandtrust.contactmanger.adapter.ContactsAdapter;
import com.techandtrust.db.ContactDataSource;

import java.util.ArrayList;

public class SearchableActivity extends AppCompatActivity {

    // ===========================================================
    // Public Constants
    // ===========================================================

    // ===========================================================
    // Private constants and Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Public methods
    // ===========================================================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        handleIntent(getIntent());
    }
    // ===========================================================
    // Private methods
    // ===========================================================
    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        Toast.makeText(this, "YARON YOREH 2", Toast.LENGTH_LONG).show();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Toast.makeText(this, "YARON YOREH asked for " + query, Toast.LENGTH_LONG).show();
            //use the query to search your data somehow
        }
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
