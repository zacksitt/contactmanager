package com.techandtrust.contactmanger.adapter;
import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.techandtrust.contactmanager.contactmanager.Contact;
import com.techandtrust.contactmanager.contactmanager.MainActivity;
import com.techandtrust.contactmanager.contactmanager.R;
import java.util.List;
/**
 * Created by ICE on 01-Jun-17.
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactViewHolder> {
    private List<Contact> contactList;
    private Context context;
    private String fontsizepref;

    public ContactsAdapter(List<Contact> contactList, Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        this.fontsizepref = sharedPref.getString(MainActivity.KEY_PREF_FONT_SIZE, "");
        this.contactList = contactList;
        this.context = context;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.contact_row, null);
        ContactViewHolder contactViewHolder = new ContactViewHolder(view);
        return contactViewHolder;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        Contact contact = new Contact();
        contact = contactList.get(position);
        final String phoneNum = contact.getPhone();
        holder.tvName.setText(contact.getName());
        holder.tvPhoneNum.setText(contact.getPhone());
        if(this.fontsizepref.equals("small")){
            holder.tvName.setTextSize(16);
            holder.tvPhoneNum.setTextSize(14);
        }else{
            holder.tvName.setTextSize(17);
            holder.tvPhoneNum.setTextSize(15);
        }

        holder.callImageBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+phoneNum));

                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context,"Phone Call Permission fail!",Toast.LENGTH_SHORT);
                    return;
                }
                context.startActivity(callIntent);
            }
        });

        holder.smsImageBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNum, null));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context,"Send SMS Permission fail!",Toast.LENGTH_SHORT);
                    return;
                }
                context.startActivity(i);
            }
        });
//        holder.itemView.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
////                String CID = contactIdByPhoneNumber(context,phoneNum);
////                Toast.makeText(context,CID, Toast.LENGTH_LONG).show();
////                Intent i = new Intent(Intent.ACTION_EDIT);
////                i.setData(Uri.parse(ContactsContract.Contacts.CONTENT_LOOKUP_URI + "/" + CID));
////                context.startActivity(i);
////                startActivityForResult(i, idEDIT_CONTACT);
//                Intent callIntent = new Intent(Intent.ACTION_CALL);
//                callIntent.setData(Uri.parse("tel:"+phoneNum));
//
//                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    Log.e("PHONE CALL","PERMISSION FAIL!!!");
//                    return;
//                }
//                context.startActivity(callIntent);
//            }
//        });
    }

    public static final String contactIdByPhoneNumber(Context ctx, String phoneNumber) {
        String contactId = null;
        if (phoneNumber != null && phoneNumber.length() > 0) {
            ContentResolver contentResolver = ctx.getContentResolver();

            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

            String[] projection = new String[] { ContactsContract.PhoneLookup._ID };

            Cursor cursor = contentResolver.query(uri, projection, null, null, null);

            if (cursor != null) {
                while (cursor.moveToNext()) {
                    contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                }
                cursor.close();
            }
        }
        return contactId;
    }
    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvPhoneNum;
        ImageView imageView;
        ImageButton callImageBtn,smsImageBtn;

        public ContactViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.ivContactImage);
            tvName = (TextView) itemView.findViewById(R.id.tvContactName);
            tvPhoneNum = (TextView) itemView.findViewById(R.id.tvPhoneNumber);
            callImageBtn = (ImageButton) itemView.findViewById(R.id.callImageButton);
            smsImageBtn = (ImageButton) itemView.findViewById(R.id.smsImageButton);
        }

    }
}
