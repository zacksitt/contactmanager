package com.techandtrust.contactmanger.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.techandtrust.contactmanager.contactmanager.MptFragment;
import com.techandtrust.contactmanager.contactmanager.OreedooFragment;
import com.techandtrust.contactmanager.contactmanager.TelenorFragment;

import java.util.ArrayList;

/**
 * Created by ICE on 01-Jun-17.
 */

public class TabPageAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> fragments = new ArrayList<>();
    ArrayList<String> tabTitles = new ArrayList<>();

    public TabPageAdapter(FragmentManager fm) {
        super(fm);
    }

    public void AddFragement(Fragment fragment,String title){
        this.fragments.add(fragment);
        this.tabTitles.add(title);
    }
    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return this.tabTitles.get(position);
    }
}
