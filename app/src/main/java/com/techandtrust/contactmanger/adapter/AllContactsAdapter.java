package com.techandtrust.contactmanger.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.techandtrust.contactmanager.contactmanager.Contact;
import com.techandtrust.contactmanager.contactmanager.MainActivity;
import com.techandtrust.contactmanager.contactmanager.R;

import java.util.List;

/**
 * Created by ICE on 07-Jun-17.
 */

public class AllContactsAdapter extends RecyclerView.Adapter<AllContactsAdapter.ContactViewHolder>{
    private List<Contact> contactList;
    private Context context;
    private String fontsizepref;

    public AllContactsAdapter(List<Contact> contactList, Context context) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        this.fontsizepref = sharedPref.getString(MainActivity.KEY_PREF_FONT_SIZE, "");
        this.contactList = contactList;
        this.context = context;
    }

    @Override
    public AllContactsAdapter.ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.allcontact_row, null);
        AllContactsAdapter.ContactViewHolder AllContactsAdapter = new AllContactsAdapter.ContactViewHolder(view);
        return AllContactsAdapter;
    }

    @Override
    public void onBindViewHolder(AllContactsAdapter.ContactViewHolder holder, int position) {
        Contact contact = new Contact();
        contact = contactList.get(position);
        final String phoneNum = contact.getPhone();
        holder.tvName.setText(contact.getName());
        holder.tvPhoneNum.setText(contact.getPhone());
        if(this.fontsizepref.equals("small")){
            holder.tvName.setTextSize(16);
            holder.tvPhoneNum.setTextSize(14);
        }else{
            holder.tvName.setTextSize(17);
            holder.tvPhoneNum.setTextSize(15);
        }
        holder.tvOperator.setText("<"+contact.getOperator()+">");
        holder.callImageBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+phoneNum));

                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context,"Phone Call Permission fail!",Toast.LENGTH_SHORT);
                    return;
                }
                context.startActivity(callIntent);
            }
        });

        holder.smsImageBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNum, null));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context,"Send SMS Permission fail!",Toast.LENGTH_SHORT);
                    return;
                }
                context.startActivity(i);
            }
        });
//        holder.itemView.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//                Intent callIntent = new Intent(Intent.ACTION_CALL);
//                callIntent.setData(Uri.parse("tel:"+phoneNum));
//
//                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    Log.e("PHONE CALL","PERMISSION FAIL!!!");
//                    return;
//                }
//                context.startActivity(callIntent);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvPhoneNum;
        TextView tvOperator;
        ImageView imageView;
        ImageButton callImageBtn,smsImageBtn;

        public ContactViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.ivContactImage);
            tvName = (TextView) itemView.findViewById(R.id.tvContactName);
            tvPhoneNum = (TextView) itemView.findViewById(R.id.tvPhoneNumber);
            tvOperator= (TextView) itemView.findViewById(R.id.tvOperator);
            callImageBtn = (ImageButton) itemView.findViewById(R.id.callImageButton);
            smsImageBtn = (ImageButton) itemView.findViewById(R.id.smsImageButton);
        }

    }
}
