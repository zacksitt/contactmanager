package com.techandtrust.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.techandtrust.contactmanager.contactmanager.MainActivity;

/**
 * Created by ICE on 02-Jun-17.
 */

public class MySqlHelper extends SQLiteOpenHelper{

    public static final String TABLE_CONTACT= "contacts";
    public static final String CONTACT_COLUMN_ID = "id";
    public static final String CONTACT_COLUMN_ORERATOR = "operator";
    public static final String CONTACT_COLUMN_NAME= "name";
    public static final String CONTACT_COLUMN_PHONE = "phone";
    public static final String CONTACT_COLUMN_IMAGE = "image";
    public static final String CONTACT_COLUMN_FAVOURITE = "favourite";

    public static final String TABLE_OPERATOR= "operator";
    public static final String OPERATOR_COLUMN_ID= "id";
    public static final String OPERATOR_COLUMN_NAME= "name";

    public static final String DATABASE_NAME ="contactmanager";
    public static final int DATABASE_VERSION =1;

    public static final String CONTACT_TABLE_CREATE ="create table " +
            TABLE_CONTACT + "(" + CONTACT_COLUMN_ID+ " integer primary key autoincrement," +
            CONTACT_COLUMN_ORERATOR + " text," + CONTACT_COLUMN_NAME+ " text," + CONTACT_COLUMN_PHONE + " text," +
            CONTACT_COLUMN_IMAGE + " text," + CONTACT_COLUMN_FAVOURITE + " integer);";

    public  static final String OPERATOR_TABLE_CREATE = "create table " +
            TABLE_OPERATOR + "(" + OPERATOR_COLUMN_ID + " integer primary key autoincrement, " +
            OPERATOR_COLUMN_NAME + " text);";

    public MySqlHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CONTACT_TABLE_CREATE);
        Log.e(this.getClass().getSimpleName(),"CREATED CONTACT TABLE");
//        db.execSQL(OPERATOR_TABLE_CREATE);
//        Log.e(this.getClass().getSimpleName(),"CREATED OPERATOR TABLE");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
