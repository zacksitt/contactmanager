package com.techandtrust.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.techandtrust.contactmanager.contactmanager.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ICE on 02-Jun-17.
 */

public class ContactDataSource {

    private SQLiteDatabase sqLiteDatabase;
    private MySqlHelper sqlHelper;

    private String[] allColumns = { sqlHelper.CONTACT_COLUMN_ID,sqlHelper.CONTACT_COLUMN_NAME,
            sqlHelper.CONTACT_COLUMN_PHONE,sqlHelper.CONTACT_COLUMN_IMAGE,sqlHelper.CONTACT_COLUMN_FAVOURITE,
            sqlHelper.CONTACT_COLUMN_ORERATOR};
    public ContactDataSource(Context context) {
        sqlHelper = new MySqlHelper(context);
    }

    public void open() throws SQLException{
        sqLiteDatabase = sqlHelper.getWritableDatabase();
    }

    public void close(){
        sqlHelper.close();
    }

    public int getCount(){
        Cursor cursor = sqLiteDatabase.query(sqlHelper.TABLE_CONTACT,allColumns,null,null,null,null,null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void clearAll(){
        sqLiteDatabase.execSQL("delete from "+ sqlHelper.TABLE_CONTACT);
    }

    public ArrayList<Contact> getContacts(String operator){

        ArrayList<Contact> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.query(sqlHelper.TABLE_CONTACT,allColumns,sqlHelper.CONTACT_COLUMN_ORERATOR+"='"+operator+"'",null,null,null,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Contact fed = cursorToContactObj(cursor);
            list.add(fed);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public ArrayList<Contact> getall(){
        ArrayList<Contact> list = new ArrayList<Contact>();
        Cursor cursor = sqLiteDatabase.query(sqlHelper.TABLE_CONTACT,allColumns,null,null,null,null,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Contact fed = cursorToContactObj(cursor);
            list.add(fed);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public Contact add(Contact cObj){

        Contact contact;
        ContentValues values = new ContentValues();
        values.put(sqlHelper.CONTACT_COLUMN_ORERATOR,cObj.getOperator());
        values.put(sqlHelper.CONTACT_COLUMN_NAME,cObj.getName());
        values.put(sqlHelper.CONTACT_COLUMN_PHONE,cObj.getPhone());
        values.put(sqlHelper.CONTACT_COLUMN_IMAGE,cObj.getImage());
        values.put(sqlHelper.CONTACT_COLUMN_FAVOURITE,cObj.getFavourite());

        long inserted_id = sqLiteDatabase.insert(sqlHelper.TABLE_CONTACT,null,values);
        Cursor cursor = sqLiteDatabase.query(sqlHelper.TABLE_CONTACT,allColumns,sqlHelper.CONTACT_COLUMN_ID+"="+inserted_id,null,null,null,null);
        cursor.moveToFirst();
        contact = cursorToContactObj(cursor);
        return contact;
    }

    private Contact cursorToContactObj(Cursor cursor) {

        Contact contact = new Contact();
        contact.setId((int) cursor.getLong(0));
        contact.setName(cursor.getString(1));
        contact.setPhone(cursor.getString(2));
        contact.setImage(cursor.getString(3));
        contact.setFavourite((int) cursor.getLong(4));
        contact.setOperator(cursor.getString(5));
        return contact;
    }
}
